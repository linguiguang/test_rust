

菜鸟文档：
https://www.runoob.com/rust/rust-async-await.html



cargo init  初始化新的项目 如果不使用cargo new的话

cargo new <project-name>：创建一个新的 Rust 项目。
cargo build ：编译当前项目。
cargo run ：编译并运行当前项目。
cargo check ：检查当前项目的语法和类型错误。
cargo test ：运行当前项目的单元测试。
cargo update ：更新 Cargo.toml 中指定的依赖项到最新版本。
cargo --help ：查看 Cargo 的帮助信息。
cargo publish ：将 Rust 项目发布到 crates.io。
cargo clean ：清理构建过程中生成的临时文件和目录。
