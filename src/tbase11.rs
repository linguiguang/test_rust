//use tokio;

use core::time;
use std::{boxed, result};
use tokio;
//use reqwest;
//

/*
要将 Rust 与 Tokio 集成起来，需要将 Tokio 添加到 Rust 项目的依赖中。在 Cargo.toml 文件中添加以下行：
[dependencies]
tokio = { version = "1.12.0", features = ["full"] }
*/


//异步函数 模拟异步任务
async fn async_task()->u32 {
    //tokio::
    tokio::time::sleep(std::time::Duration::from_secs(1)).await;
    42
}

//异步任务执行函数
async fn execute_async_task() {
    let rlt = async_task().await;
    println!("async result:{}", rlt);
}

//--------------------------------------------------------------------------------------

/* async fn fetch_url(url:&str)->Result<String, Box<dyn Error>> {
    // 使用 reqwest 发起异步 HTTP GET 请求
    let resp = get(url).await?;
    let body = resp.text().await?;
    Ok(body);
}

async fn execute_url()->Result<(), Box<dyn Error>> {
    let url = "https://jsonplaceholder.typicode.com/posts/1";
    let resp = fetch_url(url).await?;
    println!("execute url, resp:{}", resp);
    Ok(());
} */

//--------------------------------------------------------------------------------------

#[tokio::main]
async fn do_base11() {
    println!("------------------------ do base11 begin -------------------------------------");

    execute_async_task().await;

    // 创建异步运行时
    /* let rt = Runtime::new().unwrap;
    // 在异步运行时中执行异步任务
    let rst = rt.block_on(execute_url());
    match rst {
        Ok(_) => println!("async url ok");
        Err(e) => println!("async url err:{}", e)
    } */

    println!("------------------------ do base11 end -------------------------------------");
}
