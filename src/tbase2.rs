

fn do_base2() {
    println!("------------------------ do base2 begin -------------------------------------");

    let b = true;
    println!("b = {}", b);

    let c = 'r';
    println!("c = {}", c);
  
    //----------------------------函数-------------------------------
    let y = {
        let x = 3;
        x + 2
    };
    fn f(x:i32)->i32{
        return x+13;
    }
    println!("y = {}, f(2) = {}", y, f(2));

    //----------------------------if-------------------------------
    let a = 5;
    let a1 ;
    a1 = 3;
    if a < 10 {
        println!("a < 10")
    } else {
        println!("a >= 10")
    }

    //三元条件运算表达式 
    let a2 = if a > 10 {1} else if a > 5 {2} else {3};
    println!("a2 = {}", a2);

    //--------------------------while---------------------------------
    let mut a3 = 3;
    while a3 < 10 {
        a3 += 5;
    }
    println!("last a3 = {}", a3);

    let arr = [1,4,12,5,7];
    println!(">>>>>> arr:");
    //迭代式
    for e in arr.iter()  {
        print!("{}, ", e);
        //break;
    }
    println!("");

    //下标式
    for i in 0..5 {
        print!("{}-", arr[i]);
    }
    println!("");

    //loop
    let mut i = 0;
    loop {
        let e = arr[i];
        if e == 12 {
            break;
        }
        println!("loop e:{}", e);
        i += 1;
    }

    println!("------------------------ do base2 end -------------------------------------");
}
