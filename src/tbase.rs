
fn do_base() {
    println!("----------------------- do_base begin! -----------------------------------------");

    let mut a = 789; //mut:变量变得"可变"
    a = 11;
    let a: u64 = 12121; //默认32位

    const a1: i64 = 31;
    //const a11:i32 = 12;
    //let a1 = 1;

    let mut a2 = 1;
    a2 = 331;

    let s = "123";
    //s = s.len();        //不能给字符串变量赋整型值

    let fa = 2.0; //默认64位

    println!(
        ">>>>>>>>>> 55 332!{{}},{},{},{1},{0},{1},{2}, {3}, {4}",
        12,
        "22rrrf43",
        a,
        a2,
        fa * 1.3
    );
    println!("fa = {} hahaha", fa);

    let arr = (1, 3.2, 35);
    let (x, y, z) = arr;
    println!("arr.0 = {}, xyz={},{},{}", arr.0, x, y, z);

    let ass = [1, 33, 4, 5];
    println!("ass[1] = {}", ass[1]);

    let ass1: [i64; 4] = [1, 33, 4, 5];
    println!("ass1[1] = {}", ass1[2]);

    let mut ass2 = [11; 13];
    ass2[10] = 139; //改变  需要 mut
    println!("ass2[1] = {}", ass2[10]);

    println!("add() = {}", add(11, 122));
    println!("multi() = {}", mult(11, 15));
    
    println!("----------------------- do_base end! -----------------------------------------");
}

fn add(a:i32, b:i32)->i32   {
    a + b       //也可以不用return 但是末尾不能加;
    //return a + b;
}


fn mult(a:i32, b:i32)->i32   {
    return a * b;
}
