use std::{io::stdin, thread, vec};

include!("tbase.rs");
include!("tbase2.rs");
include!("tbase3.rs");
include!("tbase4.rs");
include!("tbase5.rs");
include!("tbase6.rs");
include!("tbase7.rs");
include!("tbase8.rs");
include!("tbase9.rs");
include!("tbase10.rs");
include!("tbase11.rs");

mod tbase5;     //模块 需与文件/目录名一致




fn main() {
    //命令行参数
    let args = std::env::args();
    println!("main args:{:?}", args);

    let mut str_buf = String::new();
    //等待输入 作为命令行参数
    //stdin().read_line(&mut str_buf).expect("failed to read line");
    //println!("get input args:{}", str_buf);

    do_base();
    do_base2();
    do_base3();
    do_base4();
    
    do_base5();
    tbase5::showBase5();

    do_base6();
    do_base7();
    do_base8();
    do_base9();
    do_base10();
    do_base11();
}
