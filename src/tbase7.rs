use std::{fs::{read, File}, i32, io::{self, Read}};


fn fr(i:i32)->Result<i32, bool> {
    if i > 0 {
        Ok(i)
    } else {
        Err(false)
    }
}

//如果参数无用 可以用_替代
fn gr(i:i32)->Result<i32, i16> {
    /* 
    ? 符的实际作用是将 Result 类非异常的值直接取出，如果有异常就将异常 Result 返回出去。
    所以，? 符仅用于返回值类型为 Result<T, E> 的函数，其中 E 类型必须和 ? 所处理的 Result 的 E 类型一致
    */
    let r1 = fr(i)?;
    Ok(r1)
}

fn readFile(path:&str)->Result<String, io::Error> {
    let mut f = File::open(path)?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s)
}


fn do_base7() {
    //panic!("base7 over!!!!!!!");
    println!("------------------------ do base7 begin -------------------------------------");

    {
        let f = File::open("1.txt");
        match f {
            Ok(file) => {
                println!("open 1.txt ok");
            },
            Err(err) => {
                println!("open 1.txt err");
            }
        }
    
        //let f1 = File::open("1.txt").unwrap();
        //let f2 = File::open("1.txt").expect("failed to open 1.txt");
    }
    {
        let r = fr(22);
        if let Ok(d) = r {
            println!("r == ok {}", d);
        } else {
            println!("r err");
        }
        
        let r = gr(-2);
        if let Ok(d) = r {
            println!("ggg r == ok {}", d);
        } else {
            println!("ggg r err");
        }
    }
    {
        let f = readFile("11.txt");
        match f {
            Ok(s)=>{
                println!("read 11.txt ok: {}", s);
            },
            Err(err) => {
                match err.kind() {
                    io::ErrorKind::NotFound => {
                        println!("read 11.txt, no found");
                    }
                    _ => {
                        println!("read 11.txt, unknown");
                    }
                }
            }
        }
    }
    
    println!("------------------------ do base7 end -------------------------------------");
}
