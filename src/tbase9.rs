use std::collections::{btree_map::Values, HashMap};




//fn getLongerStr(s1:&str, s2:&str) ->&str {  // 返回值引用可能会返回过期的引用
/* 
&i32        // 常规引用
&'a i32     // 含有生命周期注释的引用
&'a mut i32 // 可变型含有生命周期注释的引用
*/
fn getLongerStr<'a>(s1:&'a str, s2:&'a str) ->&'a str {
    if s1.len() > s2.len() {
        s1
    } else {
        s2
    }
}

fn getLongerStr2<'a>(s1:&'a String, s2:&'a String) ->&'a str {
    if s1.len() > s2.len() {
        s1
    } else {
        s2
    }
}

fn do_base9() {
    println!("------------------------ do base9 begin -------------------------------------");

    // ---------------------------------------- 生命周期 ----------------------------------------
    let mut r = 1;
    {
        let x = 3;
        //r = &x; //垂悬引用
    }
    println!("r = {}", r);

    let s;
    {
        let s1 = "aaff";
        let s2 = "svv";
        s = getLongerStr(s1, s2);
    }
    println!("s = {}", s);

    let ss;
    {
        let s1 = String::from("aaff");
        let s2 = String::from("svv11");
        ss = getLongerStr2(&s1, &s2);
        println!("ss = {}", ss);
    }
    /* 
    不可以编译通过，因为 s1，s2 没有 copy trait，内存里的值会在 {} 执行完后通过 drop 自动清理，
    把 println 语句移动到 {} 作用域内则可以正常运行
     */
    //println!("ss = {}", ss);
    
    // ---------------------------------------- 向量 ----------------------------------------
    {
        let mut v1:Vec<i32> = Vec::new();
        v1.push(12);
        println!("v1:{:?}", v1);

        let mut v1 = vec![2, 11, 6, 21];
        let mut v2 = vec![61, 121];
        v1.append(&mut v2);
        println!("222 v1:{:?}", v1);

        println!("v1[1] = {}, v1[2] = {}", match v1.get(1) {
            Some(val)=>val.to_string(),
            None=>"None".to_string()
        }, v1[5]);

        for i in &v1 {
            print!("---{}", i)
        }
        println!("");

        for i in &mut v1 {
            *i += 1000;
        }
        println!("333 v1 = {:?}", v1)
    }

    // ---------------------------------------- 字符串 ----------------------------------------
    {
        let mut s1 = 1.to_string();
        s1.push_str("aa你好");  //中文 一个字len=3
        s1.push('v');
        println!("s1 = {}, slice:{}, format:{}", s1, "slic".to_string(), format!("{}-{}", 11, 22));
        println!("s1 len:{}, chars:{:?}, count:{}, nth:2={:?}, [0..2]:{}", 
            s1.len(), s1.chars(), s1.chars().count(), s1.chars().nth(2), &s1[0..3]);    //[0..x]中文切断会报错
    }

    // ---------------------------------------- 映射表 ----------------------------------------
    {
        let mut m1 = HashMap::new();
        m1.insert("a", 123);
        m1.insert("b", 445);
        println!("m1:{:?}, k:a->v:{}", m1, m1.get("a").unwrap());

        for e in m1.iter() {
            print!("{:?},", e);
        }
        println!();

        //修改键值  除了覆盖 可直接修改
        if let Some(x) = m1.get_mut(&"a") {
            *x = 12334;
        }
        println!("222 m1:{:?}", m1);
    }

    println!("------------------------ do base9 end -------------------------------------");
}
