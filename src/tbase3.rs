
fn do_base3() {
    println!("------------------------ do base3 begin -------------------------------------");


    //------------------------------迭代器--------------------------------------------------
    let ve = vec![11, 4, 32, 21, 6];    //
    let sq_v: Vec<i32> = ve.iter().map(|x|x*x).collect();

    println!(">>>>>> sql_v:");
    for &e in sq_v.iter() {     //迭代器的引用
        print!("{}, ", e)
    }
    println!("");
    
    println!("sql_v = {:?}", sq_v);
    println!("");

    let mut it = ve.into_iter();    //消费迭代器 之后不再可用
    while let Some(v) = it.next() {
        print!("{}-", v)
    }
    println!("");

    let ve = vec![12, 4, 32, 21, 6];    //
    let vemods: Vec<i32> = ve.into_iter().skip_while(|x|x%2==0).collect();
    println!("vemods:{:?}", vemods);
    
    //--------------------------闭包---------------------------------
    let call = |a,b|a+b;
    let val = call(11,22);
    println!("val = {}", val);

    //移动与借用
    let x = 12;
    let call2 = move |y|x+y;
    println!("call2 y = {}, {}", call2(12), call2(32));
    //let x1 = x;
    println!("x = {}", x);  //非引用变量 所以可以通过编译

    let s = String::from("he34") ;
    let show_s = move || println!("s = {}", s);
    show_s();
    //println!("s := {}", s); //已失去所有权 无法通过编译

    //闭包可以作为参数传递给函数，也可以作为函数的返回值
    fn call_fn<F>(f:F) where F:Fn() {
        f();
    }
    call_fn(move || println!("call fn"));
    call_fn(show_s);

    let ve = vec![11, 4, 32, 21, 61];    //
    let handles = ve.into_iter().map(|x|{
        thread::spawn(move || {x*2})
    });
    for h in handles {
        let r = h.join().unwrap();
        println!("handler r = {}", r);
    }

    let square = |z|z*z;
    println!("call_sq(11) = {}", call_sq(11, square)) ;

    println!("------------------------ do base3 end -------------------------------------");

}

// 定义一个函数，接受一个闭包作为参数，将闭包应用到给定的数字上
fn call_sq<F>(n:i32, opr:F)->i32 
where 
    F:Fn(i32)->i32, 
{
    opr(n)
}
