//use std::intrinsics::mir::Return;


fn do_base4() {
    println!("------------------------ do base4 begin -------------------------------------");

    {
        let s = String::from("ddd") ;
        let s1 = s;
        //println!("s1 = {}", s);  // 错误！s 已经失效
    }
    {
        let s = String::from("2aass") ;
        let s2 = s.clone();
        println!("s2 = {}", s);
    }
    {
        let s = String::from("3423");
        fowner(s);
        //println!("s = {}", s);

        let i = 33;
        fcopy(i);
        println!("i = {}", i);
        
    }
    {
        let s = fgive();
        println!("fgive s = {}", s);
        println!("fgive s = {}", s);

        let s = fgive2();
        println!("fgive2 s = {}", s);
        println!("fgive2 s = {}", s);
        println!("s len = {}", getSize(&s));

        let rs = &s;
        println!("000 rs = {}", rs);
    }
    {
        let s = String::from("1122") ;
        let mut rs = &s;
        let s2 = s;
        
        //s2占用s所有权 输出rs会出错 需要重新赋值
        rs = &s2;
        println!("111 rs = {}", rs);   //出错

        println!("111 s2 = {}", s2);
    }
    {
        let mut s = String::from("1234") ;
        let rs = &mut s;
        rs.push_str("w");
        //rs.push_str("e");
        println!("222 rs = {}", rs);
    }
    {
        //可变引用不允许多重引用，但不可变引用可以
        let mut s = String::from("1325") ;
        
        //let rs1 = &mut s;
        //let rs2 = &mut s;   //错误
        //println!("333 rs1 = {}, rs2 = {}", rs1, rs2);

        let rs3 = &s;
        let rs4 = &s;   //正确
        println!("333 rs1 = {}, rs2 = {}", rs3, rs4);
    }
    {
        let mut s = String::from("abcvf1234");
        //注意：切片结果必须是引用类型
        let s1 = &s[0..5];
        let s2 = &s[5..9];
        //s.push_str("s");   //禁止修改
        let s3 = &s[..];
        println!("444 s1 = {}, s2 = {}, s3 = {}", s1, s2, s3);
    }

    let found = 121;
    let site1 = Site {
        domain: String::from("222"),
        //found:found
        found
    };
    //如果正在实例化的结构体有字段名称和现存变量名称一样的，可以简化书写
    println!("site1.domain = {}, found = {}", site1.domain, site1.found);
    
    site1.show();       //方法
    println!("site1->found = {}", site1.getFound());
    Site::show22(&site1);     //函数
    let domain1 = site1.domain.clone();     //所有权转让
    println!("found1 = {}", domain1);
    println!("site.domain = {}", site1.domain); //编译报错 所以需要clone()即可

    //元组结构体
    #[derive(Debug)]
    struct  Color(u32, u32, u32);
    let color1 = Color(1,2,3);
    //一定要导入调试库 #[derive(Debug)]  才能使用输出一整个结构体
    println!("color1 = {:?}", color1);

    //单元结构体:没有身体
    struct UnitStruct;
    
    println!("------------------------ do base4 end -------------------------------------");

}

fn fowner(v:String) {
    println!("fowner, s = {}", v)
} // 函数结束, 参数 some_string 在这里释放

fn fcopy(v:i32) {
    println!("fcopy, v = {}", v)
} // 函数结束, 参数 some_integer 是基本类型, 无需释放

fn fgive() -> String {
    let s = String::from("222");
    return s;
}



fn fgive2() -> String {
    let s = "44423";
    return s.to_string();
}

fn getSize(s: &String)-> usize {
    s.len()
}

//悬挂引用 被禁止
/* fn dange()->&String {
    let s = String::from("222");
    return &s;
}
 */

#[derive(Debug)]
struct Site {
    domain:String,
    found:u32
}
impl Site {
    //方法
    fn show(&self) {
        println!("site info:{:?}", self);
    }
    fn getFound(&self)->u32 {
        //return self.found;
        self.found
    }

    //函数
    fn show22(v: &Site) {
        println!("22222 site:{:?}", v);
    }
}
