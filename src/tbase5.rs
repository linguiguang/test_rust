//use std::intrinsics::mir::Return;

#[derive(Debug)]
enum Color {
    Green,
    Black
}

enum Book {
    Papery{index: u32},
    Electronic{url: String}
}

//Option 枚举类
enum Option<T> {
    Name(T),
    None
}

fn do_base5() {
    println!("------------------------ do base5 begin -------------------------------------");

    let color = Color::Green;
    println!("enum color = {:?}", color);

    //let boo1 = Book::Papery { index: (12) };
    let book2 = Book::Electronic{url: String::from("ew43540")};
    match book2 {
        Book::Papery { index }=>{
            println!("book2 is: papery");
        },
        Book::Electronic { url }=>{
            println!("book2 is: electronic");
        }
    }

    //match还可以对基础类型分支
    let x = 123;
    match x {
        123=>{println!("x = 123")},
        _=>{}
    }

    let op = Option::Name("mmui");
    let opn:Option<&str> = Option::None;
    match op {
        Option::Name(v) => {
            println!("op is name:{}", v);
        },
        Option::None => {
            println!("op is none");
        }
    }
    //let opi = Name("r");
    if let 123 = x {        //if let 语法可以认为是只区分两种情况的 match 语句的"语法糖"
        println!("x = 123 is ok");
    }
    //Book::Papery{ index }
    let book3 = Book::Electronic{url: String::from("6655t")};
    if let Book::Papery{ index } = book3 {
        println!("book3 is papery ok -- ");
    } else {
        println!("book3 is electronic ok --");
    }

    println!("------------------------ do base5 end -------------------------------------");
}


pub fn showBase5() {
    println!("call base5");
}

