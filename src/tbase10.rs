use std::{clone, rc::Rc, sync::mpsc, thread::spawn, time::Duration};



pub struct Person2 {
    id:i32,
    name:String
}

impl Person2 {
    pub fn info(&self) {
        self.hi();
        println!("id:{}, name:{}", self.id, self.name);
    }

    fn hi(&self) {
        println!("hi person2");
    }

    fn hi2(&self) {
        println!("hi person2:{}", self.id);
    }

    fn h2() {
        println!("h2 person2");
    }
}

fn spawn_f1() {
    for i in 0..5 {
        println!("spawn f1 loop:{}", i);
        thread::sleep(Duration::from_millis(1));
    }
}

//线程
fn do_spawn() {
    let h = thread::spawn(spawn_f1);
    for i in 0..3 {
        println!("spawn main loop:{}", i);
        thread::sleep(Duration::from_millis(1));
    }
    h.join().unwrap();      //main会等待其结束
}

//声明宏
macro_rules! geet {
    //匹配
    ($name:expr) => {
        //展开
        println!("hello {}", $name);
    };
}

#[derive(Debug)]
struct Data {
    value:i32
}

fn do_base10() {
    println!("------------------------ do base10 begin -------------------------------------");

    let p = Person2{id:12, name: String::from("lki")};
    Person2::h2();
    p.hi2();
    p.info();

    do_spawn();

    //收发消息
    let (tx, rx) = mpsc::channel();
    thread::spawn(move || {
        let msg = String::from("hi");
        tx.send(msg).unwrap();
    });
    let rcv = rx.recv().unwrap();
    println!("rcv msg:{}", rcv);

    //------------------------------ 宏 ---------------------------------------------
    geet!("lgg");

    //------------------------------ 智能指针 ---------------------------------------------
    //堆上分配
    let i = Box::new(12);
    //let i2 = i;
    println!("box i = {}", i);

    //多处共享
    let ri = Rc::new(33);
    let ri2 = Rc::clone(&ri);
    println!("rc i = {}, {}", ri, ri2);

    // 创建一个 Rc 智能指针，共享数据
    let d1 = Rc::new(Data{value:21});

    // 克隆 Rc 智能指针，增加数据的引用计数
    let d1c1 = Rc::clone(&d1);
    let d1c2 = Rc::clone(&d1);

    // 输出数据的值和引用计数
    println!("Data:{}", d1c1.value);
    println!("Data ref n:{}", Rc::strong_count(&d1c1));

    // 打印克隆后的 Rc 智能指针
    println!("Data c1:{:?}", d1c1);
    println!("Data c2:{:?}", d1c2);

    println!("------------------------ do base10 end -------------------------------------");
}
