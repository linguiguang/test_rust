use std::f32::consts::PI;
use std::u32;


mod persion {

    pub fn show() { //默认私有 公有需要明示
        println!("persion show")
    }

    pub mod child {
        pub fn showch() {
            println!("child showch");
        }
        pub fn show() {
            println!("persion child show")
        }
    }

    //use 关键字可以与 pub 关键字配合使用
    pub use child::showch;

    #[derive(Debug)]
    pub enum Color2 {
        Green{ind:String},
        White(u32),
        Black
    }
}

use persion::Color2;
use persion::child;

//use 冲突 用as解决
use persion::show;
use persion::child::show as chShow;


fn do_base6() {
    println!("------------------------ do base6 begin -------------------------------------");

    persion::show();
    persion::child::showch();

    let color = persion::Color2::Green { ind: String::from("gr33") };
    println!("color = {:?}", color);

    match color {
        persion::Color2::Green { ind } => {
            println!("color is green");
        },
        persion::Color2::Black => {
            println!("color is black");
        },
        persion::Color2::White(u32) => {
            println!("color is white");
        },
    }

    let color2 = Color2::White(2233);
    println!("color2 = {:?}", color2);

    persion::show();
    child::show();  //use

    //use
    show();
    chShow();

    persion::showch();


    //标准库
    println!("sin(pi/2) = {}", (PI/2.0).sin());
    
    println!("------------------------ do base6 end -------------------------------------");
}
